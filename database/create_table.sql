-- Création de la table
CREATE TABLE temperature (
    id SERIAL PRIMARY KEY,
    temperature INTEGER
);

-- DROP TABLE temperature