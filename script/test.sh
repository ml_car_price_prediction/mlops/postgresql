#!/bin/bash
db_name='test'
db_user='test'
my_table='temperature'

function execute_sql() {
  psql --tuples-only -U $db_user -d $db_name -c "$@"
}

function log() {
  printf "Log [%s]: %s\n" "$(date -u +"%Y-%m-%dT%H:%M:%SZ")" "$*"
}

numrows=$(execute_sql "SELECT count(*) FROM $my_table")

log "start counting rows"
log "${numrows} rows found"

# apt update -y && apt upgrade -y
# apt install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev
# wget https://www.python.org/ftp/python/3.11.1/Python-3.11.1.tgz 
# tar xf Python-3.11.1.tgz
# cd Python-3.11.1
# ./configure --enable-optimizations
# make
# make install