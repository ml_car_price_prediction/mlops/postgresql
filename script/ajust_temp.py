import random
import time
import psycopg2

conn = psycopg2.connect(
    host="localhost",
    database="mydatabase",
    user="myusername",
    password="mypassword"
)

start_time = time.time()

cur = conn.cursor()
for temp in range(1, 10001):
    delta = random.choice([-1, 0, 1])
    new_temp = temp + delta
    cur.execute("UPDATE temperatures SET temp = %s WHERE id = %s", (new_temp, temp))

conn.commit()

cur.close()
conn.close()

end_time = time.time()

print("Temps d'exécution : {} secondes".format(end_time - start_time))
